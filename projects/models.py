from django.db import models
from django.conf import settings
from django.contrib.auth.models import User

# Create your models here.
USER_MODEL = settings.AUTH_USER_MODEL


class Project(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField()
    members = models.ManyToManyField(User, related_name="projects")

    def __str__(self):
        return self.name
