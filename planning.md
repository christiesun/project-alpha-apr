
## Feature 1 (Done) Install dependencies
Fork and clone the starter project from Project Alpha 
Create a new virtual environment in the repository directory for the project
Activate the virtual environment
Upgrade pip
Install django
Install black
Install flake8
Install djhtml
Deactivate your virtual environment
Activate your virtual environment
Use pip freeze to generate a requirements.txt file



## Feature 2 (Done) Set up the Django project and apps
Create a Django project named tracker so that the manage.py file is in the top directory
Create a Django app named accounts and install it in the tracker Django project in the INSTALLED_APPS list
Create a Django app named projects and install it in the tracker Django project in the INSTALLED_APPS list
Create a Django app named tasks and install it in the tracker Django project in the INSTALLED_APPS list
Run the migrations
Create a super user



## Feature 3 (Done) The Project model	
Create a "Project" model with attributes:
name
description
members



## Feature 4 (Done)
Registering Project model in the admin	



## Feature 5 The Project list view (Done)
a main tag that contains:

  div tag that contains:
    an h1 tag with the content "My Projects"

    if there are no projects created, then: 
      a p tag with the content "You are not assigned to any projects"

    otherwise
      a table that has two columns, 
      The first with the header "Name" and the rows with the names of the projects
      The second with the header "Number of tasks" and nothing in those rows because we don't yet have tasks



## Feature 6 Default path redirect (Done)
Go to tracker's <urls.py>, use <RedirectView> to redirect from <""> to the name of path for the list view that you created in the previous feature <"list_projects">. Register that path a name of "home".



## Feature 7 Login page (Done)
Create <urls.py> in accounts app with path "login/" and name "login".
Also go to tracker's <urls.py> to include accounts' URL pattern with prefix "accounts/".

Create templates directory under accounts, create <registration> under templates, and create <login.html> under <registration> file

In <loing.html>, put a post form tag with method "post":
an input tag with type "text" and name "username"
an input tag with type "password" and name "password"
a button with the content "Login"



## Feature 8 Require login for Project list view (Done)
In projects <views.py>, import <LoginRequiredMixin> to protect the list view for Project model so only logged in person can access it.

Change queryset of the ProjectListView to filter the Project objects where members equals the logged in user. <def get_queryset...>



## Feature 9 Logout page (Done)
Import the LogoutView from the same module that you imported the LoginView from
Register that view in your urlpatterns list with the path "logout/" and the name "logout"

Go to tracker <settings.py>, create and set the variable LOGOUT_REDIRECT_URL to the value "login" which will redirect the logout view to the login page <LOGOUT_REDIRECT_URL = "login">



## Feature 10 Sign up page (Done)
Create a function view in accounts/views, showing the sign-up form <def signup_form(request)>.
Before that, import <UserCreationForm>, <User>, and <login>.

After you have created the user, redirect the browser "registration/signup.html" to the path registered with the name "home".

Create <signup.html> under <registration> directory with a Post form



## Feature 11 The Task model (Done)
Create a <Task> model under task app with these attributes:
name,
start_date
due_date
is_completed
project
assignee

Before make and migrate database, Delete <db.sqlite3>, and everything except <_init_.py> under "migrations" in each app. Then make and run migrations.

And createsuperuser again.



## Feature 12 Registering Task in the admin (Done)

Register the Task model with the admin so that you can see it in the Django admin site.



## Feature 13 The Project detail view	(Done)***
Create a detailview in projects <views.py>

A user must be logged in to see the view <LoginRequiredMixin>


In the projects <urls.py>, register the view with the path "<int:pk>/" and the name "show_project"

Create a template projects/detail.html:
''
an h2 tag that has the content "Tasks"

if the project has tasks {% if project.tasks.all %}, then:

a table that contains five columns with the headers "Name", "Assignee", "Start date", "Due date", and "Is completed" with rows for each task in the project

Otherwise,
a p tag with the content "This project has no tasks".


Go to project list view
The project list view should now contain an a tag to the project detail page for the indicated project title in the table's first column.
<td><a href="{% url 'show_project' project.pk %}">{{project.name}}</a></td>


The second column should now show the total number of tasks for a project.
<td>{{project.tasks.all.count}}</td>





## Feature 14 The Project create view	(Done)
In projects.<views.py>, create a view for "Project" model with the name, description, and members properties in the form and handle the form submission to create a new Project.

A person must be logged in to see the view using <LoginRequiredMixin>
If the project is successfully created, it should redirect to the detail page "Show_project" for that project.
Register that view for the path "create/" in the projects urls.py and the name "create_project"

Create an HTML template that shows the form to create a new Project </projects/create.html>
Using a form tag with method "post"

Add a link to the list view <a href="{% url 'create_project' %}">Create a new project</a>
for the Project that navigates to the new create view





## Feature 15 The Task create view (Done)
In tasks.<views.py>, create a view for "Task" model for all properties except the is_completed field

A person must be logged in to see the view using <LoginRequiredMixin>
If the project is successfully created, it should redirect to the detail page "Show_project" for that project:
## def get_success_url(self):
      return reverse("show_project", kwargs={"pk": self.object.pk})

## def get_form_kwargs(self, *args, **kwargs):
        kwargs = super(TaskCreateView, self).get_form_kwargs(
            *args, **kwargs
        )
        return kwargs


Register view in the tasks app for path "create/" and name="create_task" in tasks/urls.py
And include URL patterns from tasks app in the <tracker> project with prefix "tasks/"

Create a <create.html> under tasks' template
Add a link to create a task from the project detail page <a href="{% url 'create_task' %}">Create a new task</a>
Put a form tag with method "post":
<form method="post">
  {% csrf_token %}
  {{ form.as_p }}
  <button>Create</button>
</form>




## Feature 16 Show "My Tasks" list view (Done)***
   # a logged in person to see the tasks that are assigned to them.

Create a list view for the Task model
with the objects filtered so that the person only sees the tasks assigned to them by filtering with the assignee equal to the currently logged in user:
        def get_queryset(self):
        return Task.objects.filter(assignee=self.request.user)

The view must only be accessible by people that are logged in 
<LoginRequiredMixin>

Register that view in the tasks app for the path "mine/" and the name "show_my_tasks" in the tasks <urls.py>


Create an HTML template "tasks/list.html":
if there are tasks assigned to the person: {% if task_list|length > 0 %}
Create a table with headers "Name", "Start date", "End date", "Is completed" and 

a row for each task that is assigned to the logged in person: <tbody>
   {% for task in task_list %}...

   if the task is completed, it should show the word "Done", otherwise, it should show nothing
   {% if task.is_completed == True %}<p>Done</p>
   {% else %}...

{% else %}<p>You have no tasks</p>



## Feature 17 Completing a task (Done)
In task <views.py>, create an update view for the Task model, with field "is_completed"

it should redirect to the "show_my_tasks" URL path <success_url = reverse_lazy("show_my_tasks")>

Register the view under task/urls.py

Go to tasks/list.html, modify under is_completed=False statement:
<form method="post" action="{% url 'complete_task' task.id %}">
  {% csrf_token %}
  <input type="hidden" name="is_completed" value="True">
  <button>Complete</button>
</form>

When the person clicks that button, the task's is_completed status should be updated to True and, then, the form disappears


## Feature 18 Markdownify (Done)
  1. Install the django-markdownify package using pip (the first pip command in the instructions),
   and put it into the INSTALLED_APPS in the tracker settings.py according to:
   https://django-markdownify.readthedocs.io/en/latest/install_and_usage.html#installation

  2. In the tracker settings.py file, add the configuration setting to disable sanitation:
  https://django-markdownify.readthedocs.io/en/latest/settings.html#disable-sanitation-bleach

  3. In projects' <detail.html>, load the markdownify template library:
  https://django-markdownify.readthedocs.io/en/latest/install_and_usage.html#usage

  4. Replace the p tag and {{ project.description }} in the Project detail view with this code:
  {{ project.description|markdownify }}

  5. Use pip freeze to update your requirements.txt file:
  pip freeze > requirements.txt



## Feature 19 Navigation (Done)
First, extends 'base.html" to all templates

Add a header tag as the first child of the body tag before the main tag that contains:
a nav tag that contains

a ul tag that contains

if the person is logged in,
an li tag that contains
an a tag with an href to the "show_my_tasks" path with the content "My tasks"

an li tag that contains
an a tag with an href to the "list_projects" path with the content "My projects"

an li tag that contains
an a tag with an href to the "logout" path with the content "Logout"

Otherwise,
an li tag that contains
an a tag with an href to the "login" path with the content "Login"

an li tag that contains
an a tag with an href to the "signup" path with the content "Signup"